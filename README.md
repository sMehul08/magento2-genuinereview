# README #
Package Name : Magento_GenuineReview

Version Support : Magento 2.2 +

Setup Version : 1.0.0

### What is this repository for? ###
Magento GenuineReview allows to review product only if a customer has purchased it. Guest users or existing customers who didn't bought product won't be allowed to add review.

### How do I get set up? ###
Download zip and extract it to app/code folder and run following commands.

* php bin/magento setup:upgrade

* php bin/magento setup:static-content:deploy

* php bin/magento cache:clean
